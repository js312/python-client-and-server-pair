# Pyhton Server Application
# Author: Jared Sawyers
# Takes 'port' and 'file' name as inputs.
# Allows muliple clients to connect to it by
# spawning a separate thread for each connection.
# Any time a user connects or disconnects from the
# server, an entry in the server log is made.
# The server allows clients to send messages to each
# other, writting a log for each message, including 
# type, sender, and receiver.

import sys
import socket
import threading
import time

HEADER = 64
port = int(sys.argv[1])
SERVER = '10.0.0.244'
ADDR = (SERVER, port)
FORMAT = 'ascii'
SETTING = 'replace'
MAX_SIZE = 11
DISCONNECT_MESSAGE = 'DISCONNECT' + '\0'
close_server = False
mutex = threading.Lock()
clients = []
client_threads = []

server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)
server.settimeout(3)

log_file_name = sys.argv[2]
log_file = open(log_file_name, "w")

# Writes an entry to the log file using
# mutex to ensure two threads don't try to
# access at the same time.
def write_log(log):
    mutex.acquire()
    try:
        log_file.write(log + '\n')
    finally:
        mutex.release()

# Takes a given string and checks
# the first character in the string
# and routes it accordingly
# param conn the connection of the client
# param x the string to checked
def route_message(conn, x):
    if(x[0] == 'F'):
        timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
        log_entry = timestamp + ":" + "Client " + str(clients.index()) + ", Broadcast"
        write_log(log_entry)
        broadcast(conn, x)
    elif(x[0] == '1'):
        timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
        log_entry = timestamp + ":" + "Client " + str(clients.index(conn)) + ", Send to Next"
        write_log(log_entry)
        sendNext(conn, x)
    elif(x[0] == '0'):
        echo(conn, x)

# Iterates through the list of client
# threads, checking if the selected thread
# is the current thread before having that
# thread send the message
# param msg the message to be sent
def broadcast(conn, msg):
    for c in clients:
        if(c != conn ):
            sendMessage(c, msg)

# Sends the given message back to the client
# param msg the message to be sent
def echo(conn, msg):
    timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
    log_entry = timestamp + ": " + "Client " + str(clients.index(conn)) + ", Echo Back"
    write_log(log_entry)
    sendMessage(conn, msg)

# Gets the index of the current thread
# and then adds 1 to get the position of
# the next thread in the list then calls
# that thread to send the message
# param msg the message to be sent
def sendNext(conn, msg):
    if(len(clients) == 1):
        sendMessage(conn, msg)
    else:
        position = clients.index(conn) + 1
        if(position == len(clients)-1):
            sendMessage(clients[0], msg)
        else:   
            sendMessage(clients[position], msg)

# Sends a message to the connected client
# param msg the message to be sent
def sendMessage(conn, msg):
    conn.send(msg.encode(FORMAT, SETTING))

# Method used for handling the connection after it is created.
# Takes a message sent by the client, inverts it, then sends it back.
# param conn the connection object used for the client connection
# param addr the address of the connected client
def handle_client(conn, addr):
    print(f"[NEW CONNECTION] {addr} connected.")
    characters_processed = 0
    timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
    log = f"{timestamp}: Connection from {addr}"
    print(log)
    write_log(log)

    connected = True
    while connected:
        global DISCONNECT_MESSAGE
        msg = conn.recv(HEADER).decode(FORMAT, SETTING)
        if (msg == DISCONNECT_MESSAGE):
            connected = False
            break
        else:
            route_message(conn, msg)

    conn.close()
    
    timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
    log = f"{timestamp}: Disconnect from {addr}.\n"
    print(log)
    write_log(log)


# Method used to start the server and handle the initial connections
# and spawning threads.
# Allows a client to connect and then spawns a thread for the connection
# so that multiple clients may connect at one time. So long as the user
# has not input a command to exit, the server will continue to listen for
# connections.
def start_server():
    global close_server
    server.listen()
    print(f"[LISTENING] Server is listening on {SERVER}")

    while close_server == False:
        try:
            conn, addr = server.accept()
        except:
            continue
        clients.append(conn)
        client_thread = threading.Thread(name='client ' + str(len(client_threads)+1), target=handle_client, args=(conn, addr))
        client_thread.start()
        client_threads.append(client_thread)
        print(f"[ACTIVE CONNECTIONS] {threading.activeCount() - 2}")
    for t in client_threads:
        t.join()

# Main method of the program
# spawns a thread for the server to listen on for connections,
# then waits for the user to input exit in the console to close the server.
def main():
    global close_server
    print("[Starting] server is starting...")
    listen_thread = threading.Thread(target=start_server)
    listen_thread.start()
    if (input() == 'exit'):
        close_server = True
    listen_thread.join()
    log_file.close()

if __name__ == "__main__":
    main()
