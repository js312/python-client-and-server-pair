# Python Client aplication
# Author: Jared Sawyers
# Takes 'ipaddress' and 'port' as user inputs
# Connects to either a C or Python server, sends a user inputted message, checks that
# the message is valid, and then sends it to the server. The client also logs all 
# messages it sends or receives.

import sys
import socket
import threading
import time

HEADER = 64
FORMAT = 'ascii'
SETTING = 'replace'
DISCONNECT_MESSAGE = 'DISCONNECT\0'              #A pre agreed upon message that the server recognizes as an order to disconnect from the client
DISCONNECT = False
connection = []
mutex = threading.Lock()
respone_file_name = 'clientLog.txt'
response_file = open("clientLog.txt", "w")


# Writes an entry to the log file using
# mutex to ensure two threads don't try to
# access at the same time.
def write_log(log):
    mutex.acquire()
    try:
        response_file.write(log + '\n')
    finally:
        mutex.release()

# Takes a message as an argument, pads the length if it is less than 10 characters
# and then sends it to the server. It then receives the response from the server
# and saves it to the reversed file.
# @param msg the message to be sent to the server.
def send(msg, msg_length):
    if msg_length < 10:
        msg +=  ' ' * (10 - msg_length)
    message = msg.encode(FORMAT, SETTING)
    connection[0].send(message)


# Method used to send a disconnect message to the server
def disconnect_client():
    disconnect_message = DISCONNECT_MESSAGE.encode(FORMAT, SETTING)
    connection[0].send(disconnect_message)

# Collects user input, checks if they follow 
# and uses send() to send them to the server
def handle_input():
    global DISCONNECT
    while not DISCONNECT:
        message = input("Please enter your message: ")

        if (message == 'DISCONNECT'):
            DISCONNECT = True
            #print("Disconnet set True")
            timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
            log = "Sent:" + timestamp + " Contents:" + message
            write_log(log)
            disconnect_client()
            break
        elif (message[0] == 'F' or message[0] == '1' or message[0] == '0'):
            timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
            log = "Sent:" + timestamp + " Contents:" + message
            write_log(log)
            msg_length = len(message)
            send(message, msg_length)
        elif (message[0] != 'F' and message[0] != '1' and message[0] != '0'):
            print("Incorrect message type. Message must start with 'F', '1', or '0'.")

# Listens for messages from the server
# and displays them.
def server_listen():
    global DISCONNECT
    while not DISCONNECT:
        response = connection[0].recv(HEADER).decode(FORMAT, SETTING)
        # print(f"\n{response}")
        timestamp = time.strftime("%d/%m/%y %H:%M:%S", time.localtime())
        log = "Received:" + timestamp + " Contents:" + response
        #print("Writing response log")
        write_log(log)
        #print("Finished writing response log")

# Spawns a thread for listening to the server
# and another thread for collecting user input.
# Waits until the user inputs DISCONNECT and
# then closes the threads.
def main():
    global DISCONNECT
    server = input("Enter the server ip:")
    port = int(input("Enter the port:"))
    ADDR = (server, port)
    client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    client.connect(ADDR)
    connection.append(client)
    client_thread = threading.Thread(target=handle_input)
    client_thread.start()
    listen_thread = threading.Thread(target=server_listen)
    listen_thread.start()
    while True:
        if(DISCONNECT == True):
            break
    
    client_thread.join()
    listen_thread.join()
    
    
    print(DISCONNECT_MESSAGE)
    client.close()
    response_file.close()

if __name__ == "__main__":
    main()
